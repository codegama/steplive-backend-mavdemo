<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Meeting , App\MeetingUser;
use Exception;
use Log;
use Carbon\Carbon;

class MeetingTraker implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
          try {

        $schedule_time = date('Y-m-d H:00:00',strtotime("-1 hour"));

        $end_status = [MEETING_NONE, MEETING_SCHEDULED];

        Meeting::where('schedule_time', '<=', $schedule_time)->whereIn('status', $end_status)->chunk(100, function ($meetings) {
            
            foreach ($meetings as $meeting) {

                $meeting->status = MEETING_ENDED;

                $meeting->start_time = $meeting->start_time ?: date('Y-m-d H:i:s');

                $meeting->end_time = date('Y-m-d H:i:s');

                $meeting->save();
            
            }
        });

            Log::info("MeetingTraker Success");

        } catch(Exception $e) {

            Log::info("MeetingTraker Error".print_r($e->getMessage(), true));

        }
    }
}
