<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetingRecord extends Model
{
	protected $hidden = ['deleted_at', 'id', 'unique_id'];

	protected $appends = ['meeting_record_id', 'meeting_record_unique_id','status_formatted'];

	public function getMeetingRecordIdAttribute() {

        return $this->id;
    }

    public function getMeetingRecordUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function meeting() {

    	return $this->belongsTo(Meeting::class,'meeting_id');
    }

    public function meetingUsers() {

		return $this->hasMany(MeetingUser::class,'meeting_id');
    }

    public function getStatusFormattedAttribute() {

        return meeting_status($this->status);
    }
    
    public function userDetails() {

        return $this->belongsTo(User::class,'user_id');
    }


    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCommonResponse($query) {

        return $query->join('meetings','meetings.id','=','meeting_records.meeting_id')
                      ->select('meeting_records.*');
    }


    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = "MRT"."-".uniqid();
        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "MRT"."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

    }
}
